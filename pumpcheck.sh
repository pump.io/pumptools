#!/usr/bin/env bash

##  PumpCheck
##  Copyright 2016-2017  JanKusanagi JRR <jancoding@gmx.com>
##
##  A simple script to check availability and versions of Pump.io servers
##  Uses the 'curl' and 'whois' tools for the actual checks.
##
##  Available options:
##
##    --nocolor          Don't output color to the terminal
##    --expiration       Check domain expiration date via whois


##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 2 of the License, or
##  (at your option) any later version.
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the
##  Free Software Foundation, Inc.,
##  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.


PUBLICSERVERS=("identi.ca"
               "datamost.com"
               "mipump.es"
               "microca.st"
               "fmrl.me"
               "pumpity.net"
               "pumpdog.me"
               "1realtime.net"
               "pumpit.info"
               "pumpbuddy.us"
               "pumprock.net"
               "hotpump.net"
               "pumpyourself.com"
               "urmf.net"
               "http://awkwardly.social"
               "i.rationa.li")

PRIVATESERVERS=("e14n.com"
                "zombienet.org"
                "pump.saz.im"
                "hub.polari.us"
                "pump.ghic.org"
                "pump.strugee.net")

TOTALSERVERS=${#PUBLICSERVERS[@]}
let TOTALSERVERS=$TOTALSERVERS+${#PRIVATESERVERS[@]}


MAXREQTIME=30

REDCOLOR="\033[91m"
GREENCOLOR="\033[92m"
ENDCOLOR="\033[0m"

CHECKEXPIRATION=0

########################################################## Process parameters
while [ "$#" -ne 0 ] ; do
    if [ "$1" == "--nocolor" ]; then
        REDCOLOR=""
        GREENCOLOR=""
        ENDCOLOR=""
    fi

    if [ "$1" == "--expiration" ]; then
        CHECKEXPIRATION=1
    fi

    shift
done
########################################################## /Process parameters



    echo "********** Checking $TOTALSERVERS Pump.io servers..."
    PUMPWORKED=0
    PUMPFAILED=0


    for COUNTER in 0 1
    do
        echo

        if [ "$COUNTER" == 0 ]; then
            SERVERLIST=${PUBLICSERVERS[@]}
            echo "- ${#PUBLICSERVERS[@]} public servers (have, or had, open registration):"
        else
            SERVERLIST=${PRIVATESERVERS[@]}
            echo "- ${#PRIVATESERVERS[@]} private servers (some might allow registration upon request):"
        fi

        echo -e "=============================================================================\n"


        for SERVER in ${SERVERLIST[@]}
        do
            SCHEMA="https://"
            PRETTYSERVER="$SERVER                   "

            if [ "${PRETTYSERVER:0:7}" == "http://" ]; then
                PRETTYSERVER=${PRETTYSERVER:7}  ## Remove schema from pretty server name
                SCHEMA=""                       ## Schema already included in $SERVER
            fi


            ## Print domain up to 16 chars
            echo -e "${PRETTYSERVER:0:16}: \c"


            CURLOUTPUT=$(curl -s --max-time $MAXREQTIME -I $SCHEMA$SERVER)
            CURLEXIT=$?
            PUMPTEST=$(echo "$CURLOUTPUT"|grep "Server:")
            if [ $CHECKEXPIRATION == 1 ]; then
                WHOISOUTPUT=$(whois $SERVER |grep -i "expiry\|expiration")
            fi

            if [ "$PUMPTEST" == "" ]; then
                echo -e "$REDCOLOR-- FAILED --$ENDCOLOR\c"

                if [ $CURLEXIT == 60 ]; then
                    echo -e "  (SSL issues)\c"
                fi

                echo

                let PUMPFAILED=$PUMPFAILED+1
            else
                if [ "${PUMPTEST:0:8}" == "Server: " ]; then
                    PUMPTEST=${PUMPTEST:8}
                fi

                echo -e "$GREENCOLOR$PUMPTEST$ENDCOLOR"
                let PUMPWORKED=$PUMPWORKED+1
            fi

            if [ $CHECKEXPIRATION == 1 ]; then
                echo " ** [$WHOISOUTPUT]"
            fi
        done

        echo
    done

    echo
    echo -e "*** Check complete! \c"
    if [ $PUMPFAILED -ne 0 ]; then
        echo -e "****** $PUMPFAILED failed!\c"
        echo " ($PUMPWORKED were OK)"
    fi
    echo


